/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : tiranabusstations
Author				  : Julian & Jonid Kanini

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2011-12-22 20:04:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `drejtim`
-- ----------------------------
DROP TABLE IF EXISTS `drejtim`;
CREATE TABLE `drejtim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linja_id` int(11) NOT NULL,
  `drejtimi` varchar(50) NOT NULL,
  `pershkrim` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of drejtim
-- ----------------------------
INSERT INTO `drejtim` VALUES ('1', '1', 'Sensi Orar', 'Max. speed: 47.8km/h, avg. speed: 8.7km/h, distance: 8.237km, up: 231m, down: 241m, total sense time: 00:56:49');
INSERT INTO `drejtim` VALUES ('2', '1', 'Sensi Kunder Orar', 'Max. speed: 36.1km/h, avg. speed: 8.3km/h, distance: 8.200km, up: 299m, down: 297m, total sense time: 00:59:31');
INSERT INTO `drejtim` VALUES ('3', '2', 'Sensi Orar', 'Max. speed: 48.3km/h, avg. speed: 12.3km/h, distance: 10.505km, up: 228m, down: 238m, total sense time: 00:51:07');
INSERT INTO `drejtim` VALUES ('4', '2', 'Sensi Kunder Orar', 'Max. speed: 44.1km/h, avg. speed: 14.8km/h, distance: 10.535km, up: 184m, down: 198m, total sense time: 00:42:45');
INSERT INTO `drejtim` VALUES ('5', '3', 'Kinostudio', 'Max. speed: 37.0km/h, avg. speed: 10.9km/h, distance: 17.440km, up: 353m, down: 388m, total line time: 01:36:11');
INSERT INTO `drejtim` VALUES ('6', '3', 'Kombinat', 'Max. speed: 37.0km/h, avg. speed: 10.9km/h, distance: 17.440km, up: 353m, down: 388m, total line time: 01:36:11');
INSERT INTO `drejtim` VALUES ('7', '4', 'Porcelan', 'Max. speed: 39.3km/h, avg. speed: 9.5km/h, distance: 7.119km, up: 119m, down: 185m, total line time: 00:44:53');
INSERT INTO `drejtim` VALUES ('8', '4', 'Qender', 'Max. speed: 39.3km/h, avg. speed: 9.5km/h, distance: 7.119km, up: 119m, down: 185m, total line time: 00:44:53');
INSERT INTO `drejtim` VALUES ('9', '5', 'Laprake', 'Max. speed: 48.3km/h, avg. speed: 13.6km/h, distance: 6.852km, up: 190m, down: 213m, total time: 00:30:18');
INSERT INTO `drejtim` VALUES ('10', '5', 'Qender', 'Max. speed: 48.3km/h, avg. speed: 13.6km/h, distance: 6.852km, up: 190m, down: 213m, total time: 00:30:18');
INSERT INTO `drejtim` VALUES ('11', '7', 'Institut', 'Max. speed: 41.5km/h, avg. speed: 12.1km/h, distance: 12.517km, up: 306m, down: 286m, total line time: 01:02:04');
INSERT INTO `drejtim` VALUES ('12', '7', 'Qender', 'Max. speed: 41.5km/h, avg. speed: 12.1km/h, distance: 12.517km, up: 306m, down: 286m, total line time: 01:02:04');
INSERT INTO `drejtim` VALUES ('13', '8', 'Kopeshti Zoologjik', null);
INSERT INTO `drejtim` VALUES ('14', '8', 'Qender', '');
INSERT INTO `drejtim` VALUES ('15', '6', 'Uzina e Auto Traktoreve', 'Max. speed: 35.2km/h, avg. speed: 12.1km/h, distance: 4.369km, up: 119m, down: 62m, total time: 00:21:37 ');
INSERT INTO `drejtim` VALUES ('16', '6', 'Qender', 'Max. speed: 39.3km/h, avg. speed: 9.5km/h, distance: 7.119km, up: 119m, down: 185m, total time: 00:44:53');
INSERT INTO `drejtim` VALUES ('17', '9', 'Kristal Center', null);
INSERT INTO `drejtim` VALUES ('18', '9', 'Qender', null);
INSERT INTO `drejtim` VALUES ('19', '21', 'Sauk', 'Max. speed: 41.5km/h, avg. speed: 13.1km/h, distance: 8.616km, up: 180m, down: 178m, total line time: 00:39:20');
INSERT INTO `drejtim` VALUES ('20', '21', 'Qender', '');
INSERT INTO `drejtim` VALUES ('21', '25', 'Rinas (Aeroporti Nene Tereza)', null);
INSERT INTO `drejtim` VALUES ('22', '25', 'Qender', null);

-- ----------------------------
-- Table structure for `drejtim_stacion`
-- ----------------------------
DROP TABLE IF EXISTS `drejtim_stacion`;
CREATE TABLE `drejtim_stacion` (
  `drejtim_id` int(11) NOT NULL,
  `stacion_id` int(11) NOT NULL,
  `rend` smallint(6) DEFAULT '1000',
  PRIMARY KEY (`drejtim_id`,`stacion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of drejtim_stacion
-- ----------------------------
INSERT INTO `drejtim_stacion` VALUES ('1', '1', '10');
INSERT INTO `drejtim_stacion` VALUES ('1', '2', '20');
INSERT INTO `drejtim_stacion` VALUES ('1', '3', '30');
INSERT INTO `drejtim_stacion` VALUES ('1', '4', '40');
INSERT INTO `drejtim_stacion` VALUES ('1', '5', '50');
INSERT INTO `drejtim_stacion` VALUES ('1', '6', '60');
INSERT INTO `drejtim_stacion` VALUES ('1', '7', '70');
INSERT INTO `drejtim_stacion` VALUES ('1', '8', '80');
INSERT INTO `drejtim_stacion` VALUES ('1', '9', '90');
INSERT INTO `drejtim_stacion` VALUES ('1', '10', '100');
INSERT INTO `drejtim_stacion` VALUES ('1', '11', '110');
INSERT INTO `drejtim_stacion` VALUES ('1', '12', '120');
INSERT INTO `drejtim_stacion` VALUES ('1', '13', '130');
INSERT INTO `drejtim_stacion` VALUES ('1', '14', '140');
INSERT INTO `drejtim_stacion` VALUES ('1', '15', '150');
INSERT INTO `drejtim_stacion` VALUES ('1', '16', '160');
INSERT INTO `drejtim_stacion` VALUES ('1', '17', '170');
INSERT INTO `drejtim_stacion` VALUES ('1', '18', '180');
INSERT INTO `drejtim_stacion` VALUES ('1', '19', '190');
INSERT INTO `drejtim_stacion` VALUES ('1', '20', '200');
INSERT INTO `drejtim_stacion` VALUES ('2', '4', '180');
INSERT INTO `drejtim_stacion` VALUES ('2', '5', '160');
INSERT INTO `drejtim_stacion` VALUES ('2', '6', '150');
INSERT INTO `drejtim_stacion` VALUES ('2', '7', '140');
INSERT INTO `drejtim_stacion` VALUES ('2', '8', '130');
INSERT INTO `drejtim_stacion` VALUES ('2', '9', '120');
INSERT INTO `drejtim_stacion` VALUES ('2', '10', '110');
INSERT INTO `drejtim_stacion` VALUES ('2', '11', '100');
INSERT INTO `drejtim_stacion` VALUES ('2', '12', '90');
INSERT INTO `drejtim_stacion` VALUES ('2', '13', '80');
INSERT INTO `drejtim_stacion` VALUES ('2', '14', '70');
INSERT INTO `drejtim_stacion` VALUES ('2', '15', '60');
INSERT INTO `drejtim_stacion` VALUES ('2', '16', '55');
INSERT INTO `drejtim_stacion` VALUES ('2', '17', '50');
INSERT INTO `drejtim_stacion` VALUES ('2', '18', '40');
INSERT INTO `drejtim_stacion` VALUES ('2', '21', '10');
INSERT INTO `drejtim_stacion` VALUES ('2', '22', '20');
INSERT INTO `drejtim_stacion` VALUES ('2', '23', '30');
INSERT INTO `drejtim_stacion` VALUES ('2', '24', '170');
INSERT INTO `drejtim_stacion` VALUES ('2', '25', '190');
INSERT INTO `drejtim_stacion` VALUES ('2', '26', '200');
INSERT INTO `drejtim_stacion` VALUES ('3', '4', '50');
INSERT INTO `drejtim_stacion` VALUES ('3', '8', '170');
INSERT INTO `drejtim_stacion` VALUES ('3', '9', '180');
INSERT INTO `drejtim_stacion` VALUES ('3', '28', '20');
INSERT INTO `drejtim_stacion` VALUES ('3', '29', '30');
INSERT INTO `drejtim_stacion` VALUES ('3', '30', '40');
INSERT INTO `drejtim_stacion` VALUES ('3', '31', '60');
INSERT INTO `drejtim_stacion` VALUES ('3', '32', '70');
INSERT INTO `drejtim_stacion` VALUES ('3', '33', '80');
INSERT INTO `drejtim_stacion` VALUES ('3', '34', '90');
INSERT INTO `drejtim_stacion` VALUES ('3', '35', '100');
INSERT INTO `drejtim_stacion` VALUES ('3', '36', '110');
INSERT INTO `drejtim_stacion` VALUES ('3', '37', '120');
INSERT INTO `drejtim_stacion` VALUES ('3', '38', '130');
INSERT INTO `drejtim_stacion` VALUES ('3', '39', '140');
INSERT INTO `drejtim_stacion` VALUES ('3', '40', '150');
INSERT INTO `drejtim_stacion` VALUES ('3', '41', '160');
INSERT INTO `drejtim_stacion` VALUES ('3', '42', '190');
INSERT INTO `drejtim_stacion` VALUES ('3', '43', '200');
INSERT INTO `drejtim_stacion` VALUES ('3', '49', '10');
INSERT INTO `drejtim_stacion` VALUES ('4', '4', '170');
INSERT INTO `drejtim_stacion` VALUES ('4', '8', '50');
INSERT INTO `drejtim_stacion` VALUES ('4', '9', '40');
INSERT INTO `drejtim_stacion` VALUES ('4', '10', '30');
INSERT INTO `drejtim_stacion` VALUES ('4', '27', '220');
INSERT INTO `drejtim_stacion` VALUES ('4', '28', '200');
INSERT INTO `drejtim_stacion` VALUES ('4', '29', '190');
INSERT INTO `drejtim_stacion` VALUES ('4', '30', '180');
INSERT INTO `drejtim_stacion` VALUES ('4', '31', '160');
INSERT INTO `drejtim_stacion` VALUES ('4', '32', '150');
INSERT INTO `drejtim_stacion` VALUES ('4', '33', '140');
INSERT INTO `drejtim_stacion` VALUES ('4', '35', '130');
INSERT INTO `drejtim_stacion` VALUES ('4', '37', '110');
INSERT INTO `drejtim_stacion` VALUES ('4', '39', '80');
INSERT INTO `drejtim_stacion` VALUES ('4', '40', '70');
INSERT INTO `drejtim_stacion` VALUES ('4', '41', '60');
INSERT INTO `drejtim_stacion` VALUES ('4', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('4', '44', '20');
INSERT INTO `drejtim_stacion` VALUES ('4', '46', '120');
INSERT INTO `drejtim_stacion` VALUES ('4', '47', '210');
INSERT INTO `drejtim_stacion` VALUES ('4', '48', '100');
INSERT INTO `drejtim_stacion` VALUES ('5', '5', '80');
INSERT INTO `drejtim_stacion` VALUES ('5', '14', '130');
INSERT INTO `drejtim_stacion` VALUES ('5', '33', '60');
INSERT INTO `drejtim_stacion` VALUES ('5', '43', '100');
INSERT INTO `drejtim_stacion` VALUES ('5', '79', '10');
INSERT INTO `drejtim_stacion` VALUES ('5', '80', '20');
INSERT INTO `drejtim_stacion` VALUES ('5', '81', '30');
INSERT INTO `drejtim_stacion` VALUES ('5', '82', '40');
INSERT INTO `drejtim_stacion` VALUES ('5', '83', '50');
INSERT INTO `drejtim_stacion` VALUES ('5', '84', '70');
INSERT INTO `drejtim_stacion` VALUES ('5', '85', '90');
INSERT INTO `drejtim_stacion` VALUES ('5', '86', '110');
INSERT INTO `drejtim_stacion` VALUES ('5', '87', '120');
INSERT INTO `drejtim_stacion` VALUES ('5', '88', '140');
INSERT INTO `drejtim_stacion` VALUES ('5', '89', '150');
INSERT INTO `drejtim_stacion` VALUES ('5', '90', '160');
INSERT INTO `drejtim_stacion` VALUES ('5', '91', '170');
INSERT INTO `drejtim_stacion` VALUES ('6', '14', '50');
INSERT INTO `drejtim_stacion` VALUES ('6', '33', '120');
INSERT INTO `drejtim_stacion` VALUES ('6', '43', '80');
INSERT INTO `drejtim_stacion` VALUES ('6', '79', '170');
INSERT INTO `drejtim_stacion` VALUES ('6', '80', '160');
INSERT INTO `drejtim_stacion` VALUES ('6', '81', '150');
INSERT INTO `drejtim_stacion` VALUES ('6', '82', '140');
INSERT INTO `drejtim_stacion` VALUES ('6', '83', '130');
INSERT INTO `drejtim_stacion` VALUES ('6', '84', '110');
INSERT INTO `drejtim_stacion` VALUES ('6', '85', '90');
INSERT INTO `drejtim_stacion` VALUES ('6', '87', '60');
INSERT INTO `drejtim_stacion` VALUES ('6', '88', '40');
INSERT INTO `drejtim_stacion` VALUES ('6', '89', '30');
INSERT INTO `drejtim_stacion` VALUES ('6', '90', '20');
INSERT INTO `drejtim_stacion` VALUES ('6', '91', '10');
INSERT INTO `drejtim_stacion` VALUES ('6', '92', '70');
INSERT INTO `drejtim_stacion` VALUES ('6', '93', '100');
INSERT INTO `drejtim_stacion` VALUES ('7', '15', '30');
INSERT INTO `drejtim_stacion` VALUES ('7', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('7', '50', '20');
INSERT INTO `drejtim_stacion` VALUES ('7', '51', '40');
INSERT INTO `drejtim_stacion` VALUES ('7', '52', '50');
INSERT INTO `drejtim_stacion` VALUES ('7', '53', '60');
INSERT INTO `drejtim_stacion` VALUES ('7', '54', '70');
INSERT INTO `drejtim_stacion` VALUES ('7', '55', '80');
INSERT INTO `drejtim_stacion` VALUES ('8', '15', '50');
INSERT INTO `drejtim_stacion` VALUES ('8', '43', '80');
INSERT INTO `drejtim_stacion` VALUES ('8', '50', '70');
INSERT INTO `drejtim_stacion` VALUES ('8', '51', '40');
INSERT INTO `drejtim_stacion` VALUES ('8', '54', '20');
INSERT INTO `drejtim_stacion` VALUES ('8', '55', '10');
INSERT INTO `drejtim_stacion` VALUES ('8', '56', '30');
INSERT INTO `drejtim_stacion` VALUES ('8', '57', '60');
INSERT INTO `drejtim_stacion` VALUES ('9', '39', '80');
INSERT INTO `drejtim_stacion` VALUES ('9', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('9', '71', '20');
INSERT INTO `drejtim_stacion` VALUES ('9', '94', '30');
INSERT INTO `drejtim_stacion` VALUES ('9', '95', '40');
INSERT INTO `drejtim_stacion` VALUES ('9', '96', '50');
INSERT INTO `drejtim_stacion` VALUES ('9', '97', '60');
INSERT INTO `drejtim_stacion` VALUES ('9', '98', '70');
INSERT INTO `drejtim_stacion` VALUES ('10', '39', '10');
INSERT INTO `drejtim_stacion` VALUES ('10', '43', '80');
INSERT INTO `drejtim_stacion` VALUES ('10', '71', '70');
INSERT INTO `drejtim_stacion` VALUES ('10', '94', '60');
INSERT INTO `drejtim_stacion` VALUES ('10', '95', '50');
INSERT INTO `drejtim_stacion` VALUES ('10', '96', '40');
INSERT INTO `drejtim_stacion` VALUES ('10', '97', '30');
INSERT INTO `drejtim_stacion` VALUES ('10', '98', '20');
INSERT INTO `drejtim_stacion` VALUES ('11', '39', '40');
INSERT INTO `drejtim_stacion` VALUES ('11', '40', '30');
INSERT INTO `drejtim_stacion` VALUES ('11', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('11', '48', '50');
INSERT INTO `drejtim_stacion` VALUES ('11', '71', '20');
INSERT INTO `drejtim_stacion` VALUES ('11', '72', '60');
INSERT INTO `drejtim_stacion` VALUES ('11', '73', '70');
INSERT INTO `drejtim_stacion` VALUES ('11', '74', '80');
INSERT INTO `drejtim_stacion` VALUES ('11', '75', '90');
INSERT INTO `drejtim_stacion` VALUES ('11', '76', '100');
INSERT INTO `drejtim_stacion` VALUES ('11', '77', '110');
INSERT INTO `drejtim_stacion` VALUES ('12', '39', '90');
INSERT INTO `drejtim_stacion` VALUES ('12', '40', '100');
INSERT INTO `drejtim_stacion` VALUES ('12', '43', '120');
INSERT INTO `drejtim_stacion` VALUES ('12', '48', '80');
INSERT INTO `drejtim_stacion` VALUES ('12', '71', '110');
INSERT INTO `drejtim_stacion` VALUES ('12', '72', '70');
INSERT INTO `drejtim_stacion` VALUES ('12', '73', '50');
INSERT INTO `drejtim_stacion` VALUES ('12', '74', '40');
INSERT INTO `drejtim_stacion` VALUES ('12', '75', '30');
INSERT INTO `drejtim_stacion` VALUES ('12', '76', '20');
INSERT INTO `drejtim_stacion` VALUES ('12', '77', '10');
INSERT INTO `drejtim_stacion` VALUES ('12', '78', '60');
INSERT INTO `drejtim_stacion` VALUES ('15', '22', '20');
INSERT INTO `drejtim_stacion` VALUES ('15', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('15', '58', '30');
INSERT INTO `drejtim_stacion` VALUES ('15', '59', '40');
INSERT INTO `drejtim_stacion` VALUES ('15', '60', '50');
INSERT INTO `drejtim_stacion` VALUES ('15', '61', '60');
INSERT INTO `drejtim_stacion` VALUES ('15', '62', '70');
INSERT INTO `drejtim_stacion` VALUES ('15', '63', '80');
INSERT INTO `drejtim_stacion` VALUES ('15', '64', '90');
INSERT INTO `drejtim_stacion` VALUES ('15', '65', '100');
INSERT INTO `drejtim_stacion` VALUES ('15', '66', '110');
INSERT INTO `drejtim_stacion` VALUES ('15', '67', '120');
INSERT INTO `drejtim_stacion` VALUES ('15', '68', '130');
INSERT INTO `drejtim_stacion` VALUES ('15', '69', '130');
INSERT INTO `drejtim_stacion` VALUES ('16', '17', '130');
INSERT INTO `drejtim_stacion` VALUES ('16', '43', '150');
INSERT INTO `drejtim_stacion` VALUES ('16', '58', '120');
INSERT INTO `drejtim_stacion` VALUES ('16', '59', '110');
INSERT INTO `drejtim_stacion` VALUES ('16', '60', '100');
INSERT INTO `drejtim_stacion` VALUES ('16', '61', '90');
INSERT INTO `drejtim_stacion` VALUES ('16', '62', '80');
INSERT INTO `drejtim_stacion` VALUES ('16', '63', '70');
INSERT INTO `drejtim_stacion` VALUES ('16', '64', '60');
INSERT INTO `drejtim_stacion` VALUES ('16', '65', '50');
INSERT INTO `drejtim_stacion` VALUES ('16', '66', '40');
INSERT INTO `drejtim_stacion` VALUES ('16', '67', '30');
INSERT INTO `drejtim_stacion` VALUES ('16', '68', '20');
INSERT INTO `drejtim_stacion` VALUES ('16', '69', '10');
INSERT INTO `drejtim_stacion` VALUES ('16', '70', '140');
INSERT INTO `drejtim_stacion` VALUES ('19', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('19', '99', '20');
INSERT INTO `drejtim_stacion` VALUES ('19', '100', '30');
INSERT INTO `drejtim_stacion` VALUES ('19', '101', '40');
INSERT INTO `drejtim_stacion` VALUES ('19', '102', '50');
INSERT INTO `drejtim_stacion` VALUES ('19', '103', '60');
INSERT INTO `drejtim_stacion` VALUES ('19', '104', '70');
INSERT INTO `drejtim_stacion` VALUES ('19', '105', '80');
INSERT INTO `drejtim_stacion` VALUES ('19', '106', '90');
INSERT INTO `drejtim_stacion` VALUES ('19', '107', '100');
INSERT INTO `drejtim_stacion` VALUES ('19', '108', '110');
INSERT INTO `drejtim_stacion` VALUES ('19', '109', '120');
INSERT INTO `drejtim_stacion` VALUES ('20', '43', '100');
INSERT INTO `drejtim_stacion` VALUES ('20', '99', '90');
INSERT INTO `drejtim_stacion` VALUES ('20', '100', '80');
INSERT INTO `drejtim_stacion` VALUES ('20', '101', '70');
INSERT INTO `drejtim_stacion` VALUES ('20', '102', '60');
INSERT INTO `drejtim_stacion` VALUES ('20', '103', '50');
INSERT INTO `drejtim_stacion` VALUES ('20', '104', '40');
INSERT INTO `drejtim_stacion` VALUES ('20', '105', '30');
INSERT INTO `drejtim_stacion` VALUES ('20', '107', '20');
INSERT INTO `drejtim_stacion` VALUES ('20', '109', '10');
INSERT INTO `drejtim_stacion` VALUES ('21', '43', '10');
INSERT INTO `drejtim_stacion` VALUES ('21', '110', '100');
INSERT INTO `drejtim_stacion` VALUES ('22', '43', '100');
INSERT INTO `drejtim_stacion` VALUES ('22', '110', '10');

-- ----------------------------
-- Table structure for `grafi`
-- ----------------------------
DROP TABLE IF EXISTS `grafi`;
CREATE TABLE `grafi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nisje_stacion_id` int(11) NOT NULL,
  `destinacion_stacion_id` int(11) NOT NULL,
  `nisje_drejtim_id` int(11) DEFAULT NULL,
  `drejtim_id` int(11) DEFAULT NULL,
  `ndryshim_cmimi` smallint(6) DEFAULT NULL,
  `stacione` tinyint(4) DEFAULT '1',
  `koha` float(11,2) NOT NULL DEFAULT '0.00',
  `distanca` float(11,2) NOT NULL,
  `pesha` float(11,2) NOT NULL,
  `bus` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of grafi
-- ----------------------------
INSERT INTO `grafi` VALUES ('1', '1', '2', '1', '1', null, '1', '144.00', '510.00', '1654.00', '3');
INSERT INTO `grafi` VALUES ('2', '2', '3', '1', '1', null, '1', '104.00', '560.00', '1664.00', '3');
INSERT INTO `grafi` VALUES ('3', '3', '4', '1', '1', null, '1', '70.00', '245.00', '1315.00', '3');
INSERT INTO `grafi` VALUES ('4', '4', '5', '1', '1', null, '1', '87.00', '390.00', '1477.00', '3');
INSERT INTO `grafi` VALUES ('5', '5', '6', '1', '1', null, '1', '162.00', '455.00', '1617.00', '3');
INSERT INTO `grafi` VALUES ('6', '6', '7', '1', '1', null, '1', '173.00', '512.00', '1685.00', '3');
INSERT INTO `grafi` VALUES ('7', '7', '8', '1', '1', null, '1', '160.00', '435.00', '1595.00', '3');
INSERT INTO `grafi` VALUES ('8', '8', '9', '1', '1', null, '1', '279.00', '440.00', '1719.00', '3');
INSERT INTO `grafi` VALUES ('9', '9', '10', '1', '1', null, '1', '377.00', '530.00', '1907.00', '3');
INSERT INTO `grafi` VALUES ('10', '10', '11', '1', '1', null, '1', '266.00', '430.00', '1696.00', '3');
INSERT INTO `grafi` VALUES ('11', '11', '12', '1', '1', null, '1', '159.00', '595.00', '1754.00', '3');
INSERT INTO `grafi` VALUES ('12', '12', '13', '1', '1', null, '1', '116.00', '380.00', '1496.00', '3');
INSERT INTO `grafi` VALUES ('13', '13', '14', '1', '1', null, '1', '170.00', '435.00', '1605.00', '3');
INSERT INTO `grafi` VALUES ('14', '14', '15', '1', '1', null, '1', '81.00', '330.00', '1411.00', '3');
INSERT INTO `grafi` VALUES ('15', '15', '16', '1', '1', null, '1', '118.00', '310.00', '1428.00', '3');
INSERT INTO `grafi` VALUES ('16', '16', '17', '1', '1', null, '1', '106.00', '315.00', '1421.00', '3');
INSERT INTO `grafi` VALUES ('17', '17', '18', '1', '1', null, '1', '135.00', '280.00', '1415.00', '3');
INSERT INTO `grafi` VALUES ('18', '18', '19', '1', '1', null, '1', '276.00', '520.00', '1796.00', '3');
INSERT INTO `grafi` VALUES ('19', '19', '20', '1', '1', null, '1', '149.00', '375.00', '1524.00', '3');
INSERT INTO `grafi` VALUES ('20', '20', '1', '1', '1', null, '1', '274.00', '668.00', '1942.00', '3');
INSERT INTO `grafi` VALUES ('24', '21', '22', '2', '2', null, '1', '232.00', '635.00', '1867.00', '3');
INSERT INTO `grafi` VALUES ('25', '22', '23', '2', '2', null, '1', '203.00', '455.00', '1658.00', '3');
INSERT INTO `grafi` VALUES ('26', '23', '18', '2', '2', null, '1', '66.00', '325.00', '1391.00', '3');
INSERT INTO `grafi` VALUES ('27', '18', '17', '2', '2', null, '1', '73.00', '345.00', '1418.00', '3');
INSERT INTO `grafi` VALUES ('28', '17', '16', '2', '2', null, '1', '0.00', '0.00', '1000.00', '3');
INSERT INTO `grafi` VALUES ('29', '16', '15', '2', '2', null, '1', '0.00', '0.00', '1000.00', '3');
INSERT INTO `grafi` VALUES ('30', '15', '14', '2', '2', null, '1', '123.00', '388.00', '1511.00', '3');
INSERT INTO `grafi` VALUES ('31', '14', '13', '2', '2', null, '1', '175.00', '552.00', '1727.00', '3');
INSERT INTO `grafi` VALUES ('32', '13', '12', '2', '2', null, '1', '138.00', '350.00', '1488.00', '3');
INSERT INTO `grafi` VALUES ('33', '12', '11', '2', '2', null, '1', '139.00', '290.00', '1429.00', '3');
INSERT INTO `grafi` VALUES ('34', '11', '10', '2', '2', null, '1', '195.00', '465.00', '1660.00', '3');
INSERT INTO `grafi` VALUES ('35', '10', '9', '2', '2', null, '1', '344.00', '545.00', '1889.00', '3');
INSERT INTO `grafi` VALUES ('36', '9', '8', '2', '2', null, '1', '136.00', '450.00', '1586.00', '3');
INSERT INTO `grafi` VALUES ('37', '8', '7', '2', '2', null, '1', '225.00', '740.00', '1965.00', '3');
INSERT INTO `grafi` VALUES ('38', '7', '6', '2', '2', null, '1', '165.00', '575.00', '1740.00', '3');
INSERT INTO `grafi` VALUES ('39', '6', '5', '2', '2', null, '1', '306.00', '320.00', '1626.00', '3');
INSERT INTO `grafi` VALUES ('40', '5', '24', '2', '2', null, '1', '250.00', '345.00', '1595.00', '3');
INSERT INTO `grafi` VALUES ('41', '24', '4', '2', '2', null, '1', '198.00', '270.00', '1468.00', '3');
INSERT INTO `grafi` VALUES ('45', '4', '25', '2', '2', null, '1', '253.00', '405.00', '1658.00', '3');
INSERT INTO `grafi` VALUES ('49', '25', '26', '2', '2', null, '1', '229.00', '352.00', '1581.00', '3');
INSERT INTO `grafi` VALUES ('52', '26', '21', '2', '2', null, '1', '120.00', '425.00', '1545.00', '3');
INSERT INTO `grafi` VALUES ('56', '43', '44', '4', '4', '0', '1', '125.00', '500.00', '1625.00', '3');
INSERT INTO `grafi` VALUES ('57', '44', '10', '4', '4', '0', '1', '63.00', '250.00', '1313.00', '3');
INSERT INTO `grafi` VALUES ('58', '10', '9', '4', '4', '0', '1', '63.00', '250.00', '1313.00', '3');
INSERT INTO `grafi` VALUES ('59', '9', '8', '4', '4', '0', '1', '63.00', '250.00', '1313.00', '3');
INSERT INTO `grafi` VALUES ('60', '8', '41', '4', '4', '0', '1', '165.00', '660.00', '1825.00', '3');
INSERT INTO `grafi` VALUES ('61', '41', '40', '4', '4', '0', '1', '123.00', '490.00', '1613.00', '3');
INSERT INTO `grafi` VALUES ('62', '40', '39', '4', '4', '0', '1', '113.00', '450.00', '1563.00', '3');
INSERT INTO `grafi` VALUES ('63', '39', '48', '4', '4', '0', '1', '98.00', '390.00', '1488.00', '3');
INSERT INTO `grafi` VALUES ('64', '48', '37', '4', '4', '0', '1', '63.00', '250.00', '1313.00', '3');
INSERT INTO `grafi` VALUES ('65', '37', '46', '4', '4', '0', '1', '79.00', '315.00', '1394.00', '3');
INSERT INTO `grafi` VALUES ('66', '46', '35', '4', '4', '0', '1', '85.00', '340.00', '1425.00', '3');
INSERT INTO `grafi` VALUES ('67', '35', '33', '4', '4', '0', '1', '133.00', '530.00', '1663.00', '3');
INSERT INTO `grafi` VALUES ('68', '33', '32', '4', '4', '0', '1', '113.00', '450.00', '1563.00', '3');
INSERT INTO `grafi` VALUES ('69', '32', '31', '4', '4', '0', '1', '137.00', '546.00', '1683.00', '3');
INSERT INTO `grafi` VALUES ('70', '31', '4', '4', '4', '0', '1', '133.00', '530.00', '1663.00', '3');
INSERT INTO `grafi` VALUES ('71', '4', '30', '4', '4', '0', '1', '80.00', '320.00', '1400.00', '3');
INSERT INTO `grafi` VALUES ('72', '30', '29', '4', '4', '0', '1', '113.00', '450.00', '1563.00', '3');
INSERT INTO `grafi` VALUES ('73', '29', '28', '4', '4', '0', '1', '108.00', '430.00', '1538.00', '3');
INSERT INTO `grafi` VALUES ('74', '28', '47', '4', '4', '0', '1', '138.00', '550.00', '1688.00', '3');
INSERT INTO `grafi` VALUES ('75', '47', '27', '4', '4', '0', '1', '144.00', '575.00', '1719.00', '3');
INSERT INTO `grafi` VALUES ('76', '27', '43', '4', '4', '0', '1', '198.00', '790.00', '1988.00', '3');
INSERT INTO `grafi` VALUES ('77', '49', '28', '3', '3', '0', '1', '153.00', '610.00', '1763.00', '3');
INSERT INTO `grafi` VALUES ('78', '28', '29', '3', '3', '0', '1', '90.00', '360.00', '1450.00', '3');
INSERT INTO `grafi` VALUES ('79', '29', '30', '3', '3', '0', '1', '125.00', '500.00', '1625.00', '3');
INSERT INTO `grafi` VALUES ('80', '30', '4', '3', '3', '0', '1', '90.00', '360.00', '1450.00', '3');
INSERT INTO `grafi` VALUES ('81', '4', '31', '3', '3', '0', '1', '155.00', '620.00', '1775.00', '3');
INSERT INTO `grafi` VALUES ('82', '31', '32', '3', '3', '0', '1', '150.00', '600.00', '1750.00', '3');
INSERT INTO `grafi` VALUES ('83', '32', '33', '3', '3', '0', '1', '90.00', '360.00', '1450.00', '3');
INSERT INTO `grafi` VALUES ('84', '33', '34', '3', '3', '0', '1', '135.00', '541.00', '1676.00', '3');
INSERT INTO `grafi` VALUES ('85', '34', '35', '3', '3', '0', '1', '75.00', '301.00', '1376.00', '3');
INSERT INTO `grafi` VALUES ('86', '35', '36', '3', '3', '0', '1', '98.00', '390.00', '1488.00', '3');
INSERT INTO `grafi` VALUES ('87', '36', '37', '3', '3', '0', '1', '88.00', '350.00', '1438.00', '3');
INSERT INTO `grafi` VALUES ('88', '37', '38', '3', '3', '0', '1', '88.00', '350.00', '1438.00', '3');
INSERT INTO `grafi` VALUES ('89', '38', '39', '3', '3', '0', '1', '135.00', '540.00', '1675.00', '3');
INSERT INTO `grafi` VALUES ('90', '39', '40', '3', '3', '0', '1', '119.00', '477.00', '1596.00', '3');
INSERT INTO `grafi` VALUES ('91', '40', '41', '3', '3', '0', '1', '106.00', '422.00', '1528.00', '3');
INSERT INTO `grafi` VALUES ('92', '41', '8', '3', '3', '0', '1', '126.00', '502.00', '1628.00', '3');
INSERT INTO `grafi` VALUES ('93', '8', '9', '3', '3', '0', '1', '133.00', '530.00', '1663.00', '3');
INSERT INTO `grafi` VALUES ('94', '9', '42', '3', '3', '0', '1', '94.00', '374.00', '1468.00', '3');
INSERT INTO `grafi` VALUES ('95', '42', '43', '3', '3', '0', '1', '131.00', '525.00', '1656.00', '3');
INSERT INTO `grafi` VALUES ('96', '43', '49', '3', '3', '0', '1', '138.00', '550.00', '1688.00', '3');
INSERT INTO `grafi` VALUES ('97', '79', '80', '5', '5', null, '1', '99.00', '396.00', '1495.00', '3');
INSERT INTO `grafi` VALUES ('98', '80', '81', '5', '5', null, '1', '133.00', '530.00', '1663.00', '3');
INSERT INTO `grafi` VALUES ('99', '81', '82', '5', '5', null, '1', '84.00', '335.00', '1419.00', '3');
INSERT INTO `grafi` VALUES ('100', '82', '83', '5', '5', null, '1', '81.00', '322.00', '1403.00', '3');
INSERT INTO `grafi` VALUES ('101', '83', '33', '5', '5', null, '1', '115.00', '460.00', '1575.00', '3');
INSERT INTO `grafi` VALUES ('102', '33', '84', '5', '5', null, '1', '190.00', '760.00', '1950.00', '3');
INSERT INTO `grafi` VALUES ('103', '84', '5', '5', '5', null, '1', '135.00', '540.00', '1675.00', '3');
INSERT INTO `grafi` VALUES ('104', '5', '85', '5', '5', null, '1', '160.00', '640.00', '1800.00', '3');
INSERT INTO `grafi` VALUES ('105', '85', '43', '5', '5', null, '1', '168.00', '670.00', '1838.00', '3');
INSERT INTO `grafi` VALUES ('106', '43', '86', '5', '5', null, '1', '178.00', '713.00', '1891.00', '3');
INSERT INTO `grafi` VALUES ('107', '86', '87', '5', '5', null, '1', '272.00', '1086.00', '2358.00', '3');
INSERT INTO `grafi` VALUES ('108', '87', '14', '5', '5', null, '1', '67.00', '267.00', '1334.00', '3');
INSERT INTO `grafi` VALUES ('109', '14', '88', '5', '5', null, '1', '110.00', '440.00', '1550.00', '3');
INSERT INTO `grafi` VALUES ('110', '88', '89', '5', '5', null, '1', '105.00', '418.00', '1523.00', '3');
INSERT INTO `grafi` VALUES ('111', '89', '90', '5', '5', null, '1', '152.00', '608.00', '1760.00', '3');
INSERT INTO `grafi` VALUES ('112', '90', '91', '5', '5', null, '1', '152.00', '608.00', '1760.00', '3');
INSERT INTO `grafi` VALUES ('113', '91', '90', null, '6', null, '1', '138.00', '550.00', '1688.00', '3');
INSERT INTO `grafi` VALUES ('114', '90', '89', null, '6', null, '1', '152.00', '608.00', '1760.00', '3');
INSERT INTO `grafi` VALUES ('115', '89', '88', null, '6', null, '1', '152.00', '608.00', '1760.00', '3');
INSERT INTO `grafi` VALUES ('116', '88', '14', null, '6', null, '1', '105.00', '418.00', '1523.00', '3');
INSERT INTO `grafi` VALUES ('117', '14', '87', null, '6', null, '1', '110.00', '440.00', '1550.00', '3');
INSERT INTO `grafi` VALUES ('118', '87', '92', null, '6', null, '1', '67.00', '267.00', '1334.00', '3');
INSERT INTO `grafi` VALUES ('119', '92', '43', null, '6', null, '1', '106.00', '425.00', '1531.00', '3');
INSERT INTO `grafi` VALUES ('120', '43', '85', null, '6', null, '1', '178.00', '713.00', '1891.00', '3');
INSERT INTO `grafi` VALUES ('121', '85', '93', null, '6', null, '1', '168.00', '670.00', '1838.00', '3');
INSERT INTO `grafi` VALUES ('122', '93', '84', null, '6', null, '1', '87.00', '346.00', '1433.00', '3');
INSERT INTO `grafi` VALUES ('123', '84', '33', null, '6', null, '1', '135.00', '540.00', '1675.00', '3');
INSERT INTO `grafi` VALUES ('124', '33', '83', null, '6', null, '1', '190.00', '760.00', '1950.00', '3');
INSERT INTO `grafi` VALUES ('125', '83', '82', null, '6', null, '1', '115.00', '460.00', '1575.00', '3');
INSERT INTO `grafi` VALUES ('126', '82', '81', null, '6', null, '1', '81.00', '322.00', '1403.00', '3');
INSERT INTO `grafi` VALUES ('127', '81', '80', null, '6', null, '1', '84.00', '335.00', '1419.00', '3');
INSERT INTO `grafi` VALUES ('128', '80', '79', null, '6', null, '1', '133.00', '530.00', '1663.00', '3');
INSERT INTO `grafi` VALUES ('129', '43', '50', null, '7', null, '1', '153.00', '610.00', '1763.00', '3');
INSERT INTO `grafi` VALUES ('130', '50', '15', null, '7', null, '1', '211.00', '844.00', '2055.00', '3');
INSERT INTO `grafi` VALUES ('131', '15', '51', null, '7', null, '1', '132.00', '526.00', '1658.00', '3');
INSERT INTO `grafi` VALUES ('132', '51', '52', null, '7', null, '1', '82.00', '327.00', '1409.00', '3');
INSERT INTO `grafi` VALUES ('133', '52', '53', null, '7', null, '1', '55.00', '220.00', '1275.00', '3');
INSERT INTO `grafi` VALUES ('134', '53', '54', null, '7', null, '1', '100.00', '398.00', '1498.00', '3');
INSERT INTO `grafi` VALUES ('135', '54', '55', null, '7', null, '1', '135.00', '540.00', '1675.00', '3');
INSERT INTO `grafi` VALUES ('136', '55', '54', null, '8', null, '1', '128.00', '511.00', '1639.00', '3');
INSERT INTO `grafi` VALUES ('137', '54', '56', null, '8', null, '1', '114.00', '455.00', '1569.00', '3');
INSERT INTO `grafi` VALUES ('138', '56', '51', null, '8', null, '1', '118.00', '473.00', '1591.00', '3');
INSERT INTO `grafi` VALUES ('139', '51', '15', null, '8', null, '1', '133.00', '532.00', '1665.00', '3');
INSERT INTO `grafi` VALUES ('140', '15', '57', null, '8', null, '1', '92.00', '366.00', '1458.00', '3');
INSERT INTO `grafi` VALUES ('141', '57', '50', null, '8', null, '1', '129.00', '517.00', '1646.00', '3');
INSERT INTO `grafi` VALUES ('142', '50', '43', null, '8', null, '1', '105.00', '420.00', '1525.00', '3');
INSERT INTO `grafi` VALUES ('143', '43', '71', null, '11', null, '1', '245.00', '980.00', '2225.00', '3');
INSERT INTO `grafi` VALUES ('144', '71', '40', null, '11', null, '1', '217.00', '867.00', '2084.00', '3');
INSERT INTO `grafi` VALUES ('145', '40', '39', null, '11', null, '1', '106.00', '424.00', '1530.00', '3');
INSERT INTO `grafi` VALUES ('146', '39', '48', null, '11', null, '1', '153.00', '610.00', '1763.00', '3');
INSERT INTO `grafi` VALUES ('147', '48', '72', null, '11', null, '1', '223.00', '893.00', '2116.00', '3');
INSERT INTO `grafi` VALUES ('148', '72', '73', null, '11', null, '1', '114.00', '457.00', '1571.00', '3');
INSERT INTO `grafi` VALUES ('149', '73', '74', null, '11', null, '1', '134.00', '537.00', '1671.00', '3');
INSERT INTO `grafi` VALUES ('150', '74', '75', null, '11', null, '1', '140.00', '560.00', '1700.00', '3');
INSERT INTO `grafi` VALUES ('151', '75', '76', null, '11', null, '1', '88.00', '350.00', '1438.00', '3');
INSERT INTO `grafi` VALUES ('152', '76', '77', null, '11', null, '1', '98.00', '392.00', '1490.00', '3');
INSERT INTO `grafi` VALUES ('153', '77', '76', null, '12', null, '1', '81.00', '325.00', '1406.00', '3');
INSERT INTO `grafi` VALUES ('154', '76', '75', null, '12', null, '1', '92.00', '366.00', '1458.00', '3');
INSERT INTO `grafi` VALUES ('155', '75', '74', null, '12', null, '1', '128.00', '511.00', '1639.00', '3');
INSERT INTO `grafi` VALUES ('156', '74', '73', null, '12', null, '1', '152.00', '606.00', '1758.00', '3');
INSERT INTO `grafi` VALUES ('157', '73', '78', null, '12', null, '1', '79.00', '317.00', '1396.00', '3');
INSERT INTO `grafi` VALUES ('158', '78', '72', null, '12', null, '1', '68.00', '273.00', '1341.00', '3');
INSERT INTO `grafi` VALUES ('159', '72', '48', null, '12', null, '1', '253.00', '1011.00', '2264.00', '3');
INSERT INTO `grafi` VALUES ('160', '48', '39', null, '12', null, '1', '117.00', '468.00', '1585.00', '3');
INSERT INTO `grafi` VALUES ('161', '39', '40', null, '12', null, '1', '112.00', '448.00', '1560.00', '3');
INSERT INTO `grafi` VALUES ('162', '40', '71', null, '12', null, '1', '219.00', '876.00', '2095.00', '3');
INSERT INTO `grafi` VALUES ('163', '71', '43', null, '12', null, '1', '243.00', '972.00', '2215.00', '3');
INSERT INTO `grafi` VALUES ('164', '43', '22', null, '15', null, '1', '138.00', '552.00', '1690.00', '3');
INSERT INTO `grafi` VALUES ('165', '22', '58', null, '15', null, '1', '77.00', '307.00', '1384.00', '3');
INSERT INTO `grafi` VALUES ('166', '58', '59', null, '15', null, '1', '81.00', '322.00', '1403.00', '3');
INSERT INTO `grafi` VALUES ('167', '59', '60', null, '15', null, '1', '96.00', '382.00', '1478.00', '3');
INSERT INTO `grafi` VALUES ('168', '60', '61', null, '15', null, '1', '113.00', '452.00', '1565.00', '3');
INSERT INTO `grafi` VALUES ('169', '61', '62', null, '15', null, '1', '83.00', '333.00', '1416.00', '3');
INSERT INTO `grafi` VALUES ('170', '62', '63', null, '15', null, '1', '81.00', '325.00', '1406.00', '3');
INSERT INTO `grafi` VALUES ('171', '63', '64', null, '15', null, '1', '68.00', '270.00', '1338.00', '3');
INSERT INTO `grafi` VALUES ('172', '64', '65', null, '15', null, '1', '64.00', '256.00', '1320.00', '3');
INSERT INTO `grafi` VALUES ('173', '65', '66', null, '15', null, '1', '76.00', '303.00', '1379.00', '3');
INSERT INTO `grafi` VALUES ('174', '66', '67', null, '15', null, '1', '82.00', '328.00', '1410.00', '3');
INSERT INTO `grafi` VALUES ('175', '67', '68', null, '15', null, '1', '120.00', '478.00', '1598.00', '3');
INSERT INTO `grafi` VALUES ('176', '68', '69', null, '15', null, '1', '134.00', '534.00', '1668.00', '3');
INSERT INTO `grafi` VALUES ('177', '69', '68', null, '16', null, '1', '127.00', '507.00', '1634.00', '3');
INSERT INTO `grafi` VALUES ('178', '68', '67', null, '16', null, '1', '132.00', '528.00', '1660.00', '3');
INSERT INTO `grafi` VALUES ('179', '67', '66', null, '16', null, '1', '77.00', '307.00', '1384.00', '3');
INSERT INTO `grafi` VALUES ('180', '66', '65', null, '16', null, '1', '82.00', '329.00', '1411.00', '3');
INSERT INTO `grafi` VALUES ('181', '65', '64', null, '16', null, '1', '57.00', '227.00', '1284.00', '3');
INSERT INTO `grafi` VALUES ('182', '64', '63', null, '16', null, '1', '78.00', '313.00', '1391.00', '3');
INSERT INTO `grafi` VALUES ('183', '63', '62', null, '16', null, '1', '76.00', '303.00', '1379.00', '3');
INSERT INTO `grafi` VALUES ('184', '62', '61', null, '16', null, '1', '81.00', '322.00', '1403.00', '3');
INSERT INTO `grafi` VALUES ('185', '61', '60', null, '16', null, '1', '142.00', '569.00', '1711.00', '3');
INSERT INTO `grafi` VALUES ('186', '60', '59', null, '16', null, '1', '74.00', '295.00', '1369.00', '3');
INSERT INTO `grafi` VALUES ('187', '59', '58', null, '16', null, '1', '67.00', '269.00', '1336.00', '3');
INSERT INTO `grafi` VALUES ('188', '58', '17', null, '16', null, '1', '146.00', '582.00', '1728.00', '3');
INSERT INTO `grafi` VALUES ('189', '17', '70', null, '16', null, '1', '189.00', '756.00', '1945.00', '3');
INSERT INTO `grafi` VALUES ('190', '70', '43', null, '16', null, '1', '214.00', '854.00', '2068.00', '3');

-- ----------------------------
-- Table structure for `linja`
-- ----------------------------
DROP TABLE IF EXISTS `linja`;
CREATE TABLE `linja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emer` varchar(100) NOT NULL,
  `pershkrim` varchar(250) DEFAULT NULL,
  `cmimi` smallint(6) DEFAULT NULL,
  `frekuenca` tinyint(4) DEFAULT NULL,
  `foto` varchar(250) DEFAULT NULL,
  `s_time` varchar(5) DEFAULT '07:00',
  `e_time` varchar(5) DEFAULT '23:00',
  `ngjyra_harte` varchar(10) DEFAULT 'ffffff',
  `rend` smallint(6) DEFAULT '1000',
  `grup` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of linja
-- ----------------------------
INSERT INTO `linja` VALUES ('1', 'Unaza', null, '30', null, null, null, null, 'FF0000', '1', '1_urban');
INSERT INTO `linja` VALUES ('2', 'Tirana e Re', null, '30', null, null, null, null, '00FF00', '2', '1_urban');
INSERT INTO `linja` VALUES ('3', 'Kombinat - Kinostudio', null, '30', null, null, null, null, '00FFFF', '3', '1_urban');
INSERT INTO `linja` VALUES ('4', 'Qender - Porcelan', null, '30', null, null, null, null, 'FE8080', '4', '1_urban');
INSERT INTO `linja` VALUES ('5', 'Qender - Laprake', null, '30', null, null, null, null, '660099', '5', '1_urban');
INSERT INTO `linja` VALUES ('6', 'Qender - Uzina e Traktoreve', null, '30', null, null, null, null, 'FFFF00', '6', '1_urban');
INSERT INTO `linja` VALUES ('7', 'Qender - Institut', null, '30', null, null, null, null, 'FF9900', '7', '1_urban');
INSERT INTO `linja` VALUES ('8', 'Qender - Koopshti Zoologjik', null, '30', null, null, null, null, '0000FF', '3000', '1_urban');
INSERT INTO `linja` VALUES ('9', 'Qender - Kristal', null, '30', null, null, null, null, '94DE00', '1000', '1_urban');
INSERT INTO `linja` VALUES ('10', 'Qender - Vore', null, '40', null, null, null, null, '00FF00', '60', '2_interurban');
INSERT INTO `linja` VALUES ('11', 'Qender - Kamez', null, '40', null, null, null, null, '8E006B', '50', '2_interurban');
INSERT INTO `linja` VALUES ('12', 'Qender - Sharre', null, '40', null, null, null, null, 'FFFFFF', '80', '4_rural');
INSERT INTO `linja` VALUES ('13', 'Qender - Linze', null, '30', null, null, null, null, 'FFFFFF', '1000', '4_rural');
INSERT INTO `linja` VALUES ('14', 'Qender - Berzhite', null, '40', null, null, null, null, 'FFFFFF', '70', '4_rural');
INSERT INTO `linja` VALUES ('15', 'Qender - Tufine', null, '40', null, null, null, null, 'FFFFFF', '60', '4_rural');
INSERT INTO `linja` VALUES ('16', 'Tirane - Mjull-Bathore', null, '30', null, null, null, null, 'FFFFFF', '2000', '4_rural');
INSERT INTO `linja` VALUES ('17', 'Qender - QTU', null, '0', null, null, null, null, 'FFAAFF', '200', '3_tregtar_free');
INSERT INTO `linja` VALUES ('18', 'Qender - TEG', null, '0', null, null, null, null, 'AA5500', '500', '3_tregtar_free');
INSERT INTO `linja` VALUES ('19', 'Qender - Cassa Italia', null, '0', null, null, null, null, 'FFFFFF', '300', '3_tregtar_free');
INSERT INTO `linja` VALUES ('20', 'Qender - CityPark', null, '0', null, null, null, null, 'FFFFFF', '400', '3_tregtar_free');
INSERT INTO `linja` VALUES ('21', 'Qender - Sauk', null, '30', null, null, '07:00', '23:00', '55AA7F', '1000', '1_urban');
INSERT INTO `linja` VALUES ('22', 'Qender - Kashar', null, '40', null, null, '07:00', '23:00', 'FFFFFF', '1000', '4_rural');
INSERT INTO `linja` VALUES ('23', 'Qender - Babrru', null, '40', null, null, '07:00', '23:00', 'FFFFFF', '1000', '4_rural');
INSERT INTO `linja` VALUES ('24', 'Tirane - Ndroq', null, '40', null, null, '07:00', '23:00', 'FFFFFF', '1000', '4_rural');
INSERT INTO `linja` VALUES ('25', 'Qender - Rinas (Express)', null, '250', null, null, '07:00', '23:00', '999999', '1000', '1_urban');

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `varname` varchar(25) NOT NULL DEFAULT '',
  `description` varchar(150) NOT NULL DEFAULT '',
  `setting` varchar(250) NOT NULL DEFAULT '',
  `optionorder` int(3) NOT NULL DEFAULT '0',
  `section` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'Titulli i faqes', 'titfaqe', 'Titulli i faqes do te shfaqet ne krye, ne header dhe ne footer', 'Tirana Bus Stations', '1', '1');
INSERT INTO `settings` VALUES ('2', 'Adresa URL e faqes', 'domain', 'Adresa web e faqes psh http://www.shqiperiacom.info', 'http://192.168.1.56:8080', '2', '1');
INSERT INTO `settings` VALUES ('3', 'Direktoria ku do te behen upload filet!', 'UPLOAD_PATH', 'Direktoria ku do te behen upload filet!', 'C:\\wamp\\www\\tiranabus\\', '3', '1');
INSERT INTO `settings` VALUES ('4', 'Aksesimi i imazheve nga nje URL', 'IMAGE_BASE', 'Adresa web nga aksesohen imazhet', 'http://http://192.168.1.56:8080/spse/', '4', '1');
INSERT INTO `settings` VALUES ('5', 'Direktoria e fotografive', 'FOTO_DIR', 'Ne kete direktori ndodhen fotot nen domain', 'foto/', '5', '1');
INSERT INTO `settings` VALUES ('6', 'Direktoria e mini-fotografive', 'FOTO_THUMBS_DIR', 'Ne kete direktori ndodhen fotot nen domain. Behet fjale per fotot e vogla', 'foto_thumbs/', '6', '1');
INSERT INTO `settings` VALUES ('7', 'Emri i kompanise qe ka marre softin', 'COMPANY_NAME', 'Emri i kompanise qe ka marre softin', 'TiranaBusStations', '7', '1');
INSERT INTO `settings` VALUES ('8', 'Direktoria ku do gjenden skedaret per download', 'SKEDAR_DIR', 'Direktoria nga do behen download skedaret', 'skedaret/', '8', '1');
INSERT INTO `settings` VALUES ('9', 'Direktoria ku do ruhen mini-fotografite e galerise', 'THUMBS_DIR_GALLERY', 'Direktoria ku do ruhen mini-fotografite e galerise', 'galeri_thumbs/', '9', '1');
INSERT INTO `settings` VALUES ('10', 'Direktoria ku do ruhen fotografite e galerise', 'PHOTOGAL_DIR_GALLERY', 'Direktoria ku do ruhen fotografite e galerise', 'galeri/', '10', '1');
INSERT INTO `settings` VALUES ('11', 'Numri i artikujve ne faqe te pare', 'NR_ARTS_FQ1', 'Nr artikuj faqe pare', '4', '11', '1');
INSERT INTO `settings` VALUES ('12', 'Gjeresia maksimale e fotos ne pixel', 'FOTO_MAX_WIDTH', 'Gjeresia maksimale e fotos ne pixel', '150', '12', '1');
INSERT INTO `settings` VALUES ('13', 'Gjatesia maksimale e fotos ne pixel', 'FOTO_MAX_HEIGHT', 'Gjeresia maksimale e fotos ne pixel', '150', '13', '1');
INSERT INTO `settings` VALUES ('14', 'Gjeresia maksimale e mini-fotos ne pixel', 'FOTOTHUMB_MAX_WIDTH', 'Gjeresia maksimale e mini-fotos ne pixel', '79', '14', '1');
INSERT INTO `settings` VALUES ('15', 'Gjatesia maksimale e mini-fotos ne pixel', 'FOTOTHUMB_MAX_HEIGHT', 'Gjeresia maksimale e mini-fotos ne pixel', '79', '15', '1');
INSERT INTO `settings` VALUES ('16', 'Graphic Template', 'TEMPLATENAME', 'This is the graphic design of the webpage', 'goflexible', '17', '2');
INSERT INTO `settings` VALUES ('17', 'Path i faqeve te paraprogramuara', 'PATH_PREPROGS', 'Path i faqeve te paraprogramuara ne server', 'preprogs/', '16', '1');
INSERT INTO `settings` VALUES ('18', 'English', 'gj2', 'English', 'en', '0', '0');
INSERT INTO `settings` VALUES ('19', 'Shqip', 'gj1', 'Shqip', 'al', '0', '0');

-- ----------------------------
-- Table structure for `stacion`
-- ----------------------------
DROP TABLE IF EXISTS `stacion`;
CREATE TABLE `stacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emer` varchar(100) NOT NULL,
  `vendodhje` varchar(100) DEFAULT NULL,
  `pershkrim` varchar(250) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `rend` smallint(6) DEFAULT '1000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stacion
-- ----------------------------
INSERT INTO `stacion` VALUES ('1', 'Parku Rinia', '19.817839, 41.323799', 'Kryqezimi Bulevardi \"Zhan D\'Ark\" - Rruga \"Deshmoret e 4 Shkurtit\"<br>Kompleksi \"Taiwan\", Pallatet \"Shallvare\", Pallatet \"Agimi\", Parku Rinia, Fillimi i Bllokut, Ambasada Ruse, Sky Tower, Hotel \"Dajti\", Lulishtja e \"3 Vellezerve Frasheri\"', null, '10');
INSERT INTO `stacion` VALUES ('2', 'Ish Ekspozita', '19.812919, 41.322963', 'Kryqezimi Bulevardi \"Gjergj Fishta\" - Rruga \"Sami Frasheri\"<br>Drejtoria e Policise, Ish Ekspozita \"Shqiperia Sot\", Pallatet \"Italiane\"', null, '20');
INSERT INTO `stacion` VALUES ('3', 'Gjykata e Tiranes', '19.807085, 41.32204', 'Kryqezimi Bulevardi \"Gjergj Fishta\" - Rruga \"Nikolla Lena\"<br>Gjykata e Tiranes, Shkolla \"Pjeter Budi\", Shkolla \"Vasil Shanto\"', null, '30');
INSERT INTO `stacion` VALUES ('4', 'Shkolla Vasil Shanto', '19.805342, 41.321947', 'Kryqezimi Bulevardi \"Gjergj Fishta\" - Rruga \"Muhamet Gjollesha\"<>Shkolla Vasil Shanto,  Fundi i Rruges \"Myslym Shyri\"', null, '40');
INSERT INTO `stacion` VALUES ('5', '21 Dhjetori', '19.803898, 41.324652', 'Kryqezimi Rruga \"Muhamet Gjollesha\" - Rruga e \"Kavajes\"<br>Kryqezimi i 21 Dhjetorit, Shkolla \"Sabaudin Gabrani\", Fundi i Rruges \"Myslym Shyri\"', null, '50');
INSERT INTO `stacion` VALUES ('6', 'Inxhinieria e Ndertimit', '19.802633, 41.328027', 'Kryqezimi Rruga \"Muhamet Gjollesha\" - Rruga \"Artan Lenja\"<br>Fakulteti i Inxhinierise se Ndertimit, Rruga \"Artan Lenja\", Tregu i Pajisjeve Elektrike', null, '60');
INSERT INTO `stacion` VALUES ('7', 'Zogu i Zi', '19.804288, 41.331753', 'Rreth-rrotullimi i \"Zogut te Zi\", Sheshi \"Karl Topia\"<br>Dalja 1. Rruga e \"Durresit\"<br>Dalja 2. Rruga \"Asim Vokshi\"<br>Dalja 3. Rruga \"Dritan Hoxha\"', null, '70');
INSERT INTO `stacion` VALUES ('8', 'Don Bosko', '19.807072, 41.317725', 'Kryqezimi Rruga \"Asim Vokshi\" - Rruga \"Don Bosko\"<br>Fillimi i Rruges \"Don Bosko\", Kompleksi \"Karl Topia\", Zogu i Zi', null, '80');
INSERT INTO `stacion` VALUES ('9', 'Burgu', '19.811451, 41.335128', 'Kryqezimi Rruga \"Asim Vokshi\" - Rruga \"Jordan Misja\"<br>Shkolla Harry Fultz, Drejtoria e Arkivit te Shtetit, Drejtoria e Hipotekes', null, '90');
INSERT INTO `stacion` VALUES ('10', 'Stacioni i Trenit', '19.816741, 41.335978', 'Kryqezimi Rruga \"Reshit Petrela\" - Bulevardi \"Zogu i I\"<br>Stacioni i Trenit, Materniteti \"Mbreteresha Geraldine\", Fakulteti i Shkencave te Natyres', null, '100');
INSERT INTO `stacion` VALUES ('11', 'Dispanceria', '19.804713, 41.321213\r\n', 'Kryqezimi Rruga \"Reshit Petrela\" - Rruga \"Siri Kodra\" - Rruga \"Ferit Xhajko\"', null, '110');
INSERT INTO `stacion` VALUES ('12', 'Tregu i Madh', '19.825610, 41.340014', 'Kryqezimi Rruga \"Ferit Xhajko\" - Rruga \"5 Maji\"<br>Tregu i Madh', null, '120');
INSERT INTO `stacion` VALUES ('13', 'Farmacia 10', '19.829343, 41.339735', 'Kryqezimi Rruga \"Ferit Xhajko\" - Rruga e \"Dibres\" - Rruga \"Bardhyl\"<br>Qendra Spitalore Universitare, Farmacia Nr. 10, Tregu i Madh, Medreseja', null, '130');
INSERT INTO `stacion` VALUES ('14', 'Spitalet', '19.831402, 41.336787', 'Kryqezimi Rruga \"Bardhyl\" - Rruga \"Kongresi i Manastirit\"<br>Raiffeisen Bank - Dega \"Rruga Bardhyl\", Rruga \"4 Deshmoret\", Shkolla \"4 Deshmoret\", Shkolla \"Andon Zako Cajupi\", IKEDA', null, '140');
INSERT INTO `stacion` VALUES ('15', 'Rruga Bardhyl', '19.832853, 41.334819', 'Kryqezimi Rruga \"Bardhyl\" - Rruga \"Qemal Stafa\"<br>Xhamlleku, Shkolla \"Kongresi i Lushnjes\"', null, '150');
INSERT INTO `stacion` VALUES ('16', 'Rozafa', '19.833988, 41.332802', 'Kryqezimi Rruga \"Arkitekt Kasemi\" - Rruga \"Hoxha Tasim\"', null, '160');
INSERT INTO `stacion` VALUES ('17', 'Berryli', '19.833959, 41.330404', 'Kryqezimi Bulevardi \"Zhan D\'Ark\" - Rruga \"Arkitekt Kasemi\"<br>Kryqezimi i Berrylit, Materniteti \"Koco Gliozheni\"', null, '170');
INSERT INTO `stacion` VALUES ('18', 'Ministria e Jashtme', '19.831757, 41.329559', 'Bulevardi \"Zhan D\'Ark\"<br>Ministria e Jashtme, Ministria e Shendetesise, Drejtoria e Pergjithshme e Policise se Shtetit', null, '180');
INSERT INTO `stacion` VALUES ('19', 'ATSH', '19.827258, 41.327222', 'Bulevardi \"Zhan D\'Ark\"<br>ATSH - Agjensia Telegrafike Shqiptare, Shkolla \"Ismail Qemali\", Shkolla \"1 Maji\", Xhamia e Tabakeve, Gjykata e Apelit, Laboratori i Kriminalistikes, Ura e Tabakeve', null, '190');
INSERT INTO `stacion` VALUES ('20', 'Parlamenti', '19.824734, 41.325248', 'Kryqezimi Bulevardi \"Zhan D\'Ark\" - Rruga \"George W Bush\"<br>Parlamenti, Parku i Parlamentit, Qendra Kulturore e Forcave te Armatosura, Rruga e Elbasanit, Lulishte 1 Maji, Shkolla e Baletit, Parku \"7 Xhuxhat\", ETC, Katedralja e Shen Palit', null, '200');
INSERT INTO `stacion` VALUES ('21', 'Sky Tower', null, null, null, '210');
INSERT INTO `stacion` VALUES ('22', 'Shkolla e Baletit', null, null, null, '200');
INSERT INTO `stacion` VALUES ('23', 'Ali Demi', null, null, null, '190');
INSERT INTO `stacion` VALUES ('24', 'Dispeceria e Unazes', null, null, null, '45');
INSERT INTO `stacion` VALUES ('25', 'Lulishte Cajupi', null, null, null, '30');
INSERT INTO `stacion` VALUES ('26', 'Kinema Agimi', null, null, null, '10');
INSERT INTO `stacion` VALUES ('27', 'Piramida', null, null, null, '10');
INSERT INTO `stacion` VALUES ('28', 'Libri Universitar', null, null, null, '20');
INSERT INTO `stacion` VALUES ('29', 'Stadiumi Dinamo', null, null, null, '30');
INSERT INTO `stacion` VALUES ('30', 'Komuna e Parisit', null, null, null, '40');
INSERT INTO `stacion` VALUES ('31', 'Parku i Autobuzave', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('32', 'Pallati me Shigjeta', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('33', 'Shkolla Hoteleri Turizem', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('34', 'Kryqezimi Milto Sotir Gura', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('35', 'Astiri', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('36', 'Kryqezimi Kristaq Mone', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('37', 'Kryqezimi Tahir Dizdari', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('38', 'Dogana', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('39', 'Lapraka', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('40', 'FIAT', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('41', 'Pallati i Sporteve', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('42', 'Fakulteti i Shkencave te Natyres', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('43', 'Qender', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('44', 'Materniteti Nena Mbretereshe', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('46', 'Kryqezimi Aleksandri i Madh', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('47', 'Sheshi Nene Tereza', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('48', 'Rrethrrotullimi i Doganes', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('49', 'Kullat Binjake', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('50', 'Sheshi Avni Rustemi', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('51', 'Kryqezimi Alibal Pasha', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('52', 'Profarma', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('53', 'Kryqezimi Odhise Paskali', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('54', 'Kryqezimi Marie Kraja', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('55', 'Porcelani', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('56', 'Kryqezimi Gjoleke Kokona', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('57', 'Shkolla Hoxha Tasim', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('58', 'Shkolla 1 Maji', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('59', 'Shkolla Ali Demi', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('60', 'Fusha Ali Demi', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('61', 'Shkolla Kushtrimi i Lirise', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('62', 'Kryqezimi Kristaq Cipo', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('63', 'Poligrafiku', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('64', 'Ura e Poligrafikut', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('65', 'Kryqezimi Fatmir Agalliu', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('66', 'Kryqezimi Aleksander Konda', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('67', 'Shkolla Androkli Kostallari', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('68', 'Shkolla Abdulla Keta', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('69', 'Uzina e Auto Traktoreve', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('70', 'Ura e Tabakeve', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('71', 'Gjimnazi Qemal Stafa', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('72', 'Kthesa e Kamzes', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('73', 'Rezidenca Fortesa', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('74', 'Kryqezimi Lunxheria', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('75', 'Kryqezimi Demokracia', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('76', 'Kryqezimi Gramozi', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('77', 'Universiteti Bujqesor', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('78', 'Spitali Hygeia', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('79', 'Kombinat', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('80', 'Gjimnazi Myslym Keta', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('81', 'Plus', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('82', 'Birra Tirana', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('83', 'Universal Hospital', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('84', 'NSHRAK', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('85', 'Kisha Katolike', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('86', '9 Kateshet', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('87', 'Medreseja', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('88', 'Spitali Amerikan II', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('89', 'Shkolla e Bashkuar', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('90', 'Komisariati Nr 4', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('91', 'Kinostudio', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('92', 'INSIG', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('93', 'Muzeu i Shkencave', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('94', 'Kompleksi Gener 2', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('95', 'Qendra Don Bosko', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('96', 'Parku i Autobuseve Don Bosko', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('97', 'Ish Uzina e Kepuceve', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('98', 'Spitali Ushtarak', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('99', 'Parku 7 Xhuxhat', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('100', 'Liceu Jordan Misja', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('101', 'Fakulteti i Histori-Filologjise', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('102', 'Ish Calvin', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('103', 'Sofra e Ariut', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('104', 'Pallati i Brigadave', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('105', 'Varrezat e Deshmoreve te Kombit', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('106', 'Akademia e Policise', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('107', 'Batalioni i Policise Ushtarake', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('108', 'Xhamia e Saukut', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('109', 'Sauk', null, null, null, '1000');
INSERT INTO `stacion` VALUES ('110', 'Aeroporti Nene Tereza (Rinas)', null, null, null, '1000');
