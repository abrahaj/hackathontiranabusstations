<?php
/*
* Author: julian kanini
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<script src="javascript/functions.js" type="text/javascript"></script>
<title>Tirana Bus Stations</title>
<meta content="keyword1,keyword2,keyword3" name="keywords" />
<meta content="Description of your page" name="description" />
</head>

<body>

<div id="topbar" class="transparent">
	<div id="title">Rreth Nesh</div>
<?php
include("mainmenu.php");
?>

<div id="content">
	<span class="graytitle">Kush jemi ne?</span>
	<ul class="pageitem">
		<li class="textbox">
			<span class="header">Rreth Nesh</span>
				<li class="menu">
						<a href="http://www.linkedin.com/in/juliankanini">
							<img alt="Description" src="thumbs/contacts.png" />
							<span class="name">Julian Kanini</span>
							<span class="arrow"></span>
						</a>
					</li>
					<li class="menu">
						<a href="http://www.linkedin.com/pub/jonid-kanini/3b/a47/b">
							<img alt="Description" src="thumbs/contacts.png" />
							<span class="name">Jonid Kanini</span>
							<span class="arrow"></span>
						</a>
					</li>
		</li>
	
		<li class="menu"> 
			<a class="noeffect" href="mailto:jonid.kanini@gmail.com?cc=julian.kanini@gmail.com&amp;subject=Pershendetje Tirana Bus Stations!&amp;body=Pershendetje Zhvilluesit e Tirana Bus Stations,"> 
			<img alt="mail" src="thumbs/mail.png" /><span class="name">Na dergoni nje Email :)</span><span class="arrow"></span></a>
		</li>
	</ul>
		
	
	<ul class="pageitem">
		<li class="textbox">
			<span class="header">Rreth Projektit</span>
			<p>Our aim is to provide users with a interactive bus station map and orientation while getting around Tirana, Albania. This app is still under severe development phases and is not fully ready, but you can get a preview of its features by downloading the app.</p>
		</li>
	</ul>
	
</div>

<div id="footer">
	<!-- Support iWebKit by sending us traffic; please keep this footer on your page, consider it a thank you for my work :-) -->
	<a class="noeffect" href="http://www.juliankanini.com/">iPhone / Android site powered by J.Kanini<sup>2</sup>.</a></div>

</body>

</html>
