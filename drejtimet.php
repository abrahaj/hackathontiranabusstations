<?php
/*
* Author: julian kanini
*/	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<script src="javascript/functions.js" type="text/javascript"></script>
<title>Tirana Bus Stations</title>
<meta content="keyword1,keyword2,keyword3" name="keywords" />
<meta content="Description of your page" name="description" />



<script type="text/javascript" src="jquery/jquery.js"></script>
<script type='text/javascript' src='jquery/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='jquery/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='jquery/thickbox-compressed.js'></script>
<script type='text/javascript' src='jquery/jquery.autocomplete.js'></script>

<?php include_once 'localdata.php'; ?>

<link rel="stylesheet" type="text/css" href="jquery/jquery.autocomplete.css" />

	
<script type="text/javascript">
$().ready(function() {
	
	$("#nisje").autocomplete(nisje, {
		minChars: 0,
		max: 12,
		autoFill: false,
		mustMatch: false,
		matchContains: false,
		scrollHeight: 220,
	});
		
	$("#destinacion").autocomplete(destinacion, {
		minChars: 0,
		max: 12,
		autoFill: false,
		mustMatch: false,
		matchContains: false,
		scrollHeight: 220,
	});
});


</script>		
	
</head>

<body>

<div id="topbar" class="transparent">
	<div id="title">Drejtimet</div>
<?php
include("mainmenu.php");
?>



<div id="content">
	<span class="graytitle">Rruga me e shkurter</span>
	<ul class="pageitem">
		<li class="textbox">
			<span class="header"></span>
			<p>Gjej rrugen me te shkurter per desinacionin tend...</p>
		</li>		
	</ul>
	
	
	<form action="drejtimet_src.php" method="POST">
		<ul class="pageitem">
			<li class="bigfield"><input placeholder="Nisja" name="nisje" id="nisje" type="text" /></li>
		<li class="bigfield"><input placeholder="Destinacioni" name="destinacion" id="destinacion" type="text" /></li> 
		</ul>

		<ul class="pageitem">
			<li class="button"><input name="name" type="submit" value="Kerko"/></li>
		</ul>
	</form>
	
	
	
	
</div>




<div id="footer">
	<!-- Support iWebKit by sending us traffic; please keep this footer on your page, consider it a thank you for my work :-) -->
	<a class="noeffect" href="http://www.juliankanini.com/">iPhone / Android site powered by J.Kanini<sup>2</sup>.</a></div>

</body>

</html>
