<?php
/*
* Author: julian kanini
*/
require_once("dijkstra.php");
require_once("sysconfig.php");

$_SESSION["graf"] = new Graph();
require_once("dijkstra_load.php"); 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />

<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<link rel="stylesheet" type="text/css" href="./imetro/imetro.css">

<script src="javascript/functions.js" type="text/javascript"></script>
<title>Tirana Bus Stations</title>
<meta content="keyword1,keyword2,keyword3" name="keywords" />
<meta content="Description of your page" name="description" />
</head>

<body>

<div id="topbar" class="transparent">
	<div id="title">Rruga me e Shkurter</div>
<?php
include("mainmenu.php");

$mtd=$_GET["mtd"];
$nisje="";
$destinacion="";


if(isset($_POST["nisje"]) && isset($_POST["destinacion"]))
{
	$nisje=$_POST["nisje"];
	$destinacion=$_POST["destinacion"];
}
else if (isset($_GET["n"]) && isset($_GET["d"]))
{
	$nisje=$_GET["n"];
	$destinacion=$_GET["d"];
}

$path=displayDrejtim($nisje, $destinacion);

function sec2hms ($sec, $padHours = false) 
  {

    // start with a blank string
    $hms = "";
    
    // do the hours first: there are 3600 seconds in an hour, so if we divide
    // the total number of seconds by 3600 and throw away the remainder, we're
    // left with the number of hours in those seconds
    $hours = intval(intval($sec) / 3600); 

    // add hours to $hms (with a leading 0 if asked for)
    $hms .= ($padHours) 
          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":"
          : $hours. ":";
    
    // dividing the total seconds by 60 will give us the number of minutes
    // in total, but we're interested in *minutes past the hour* and to get
    // this, we have to divide by 60 again and then use the remainder
    $minutes = intval(($sec / 60) % 60); 

    // add minutes to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";

    // seconds past the minute are found by dividing the total number of seconds
    // by 60 and using the remainder
    $seconds = intval($sec % 60); 

    // add seconds to $hms (with a leading 0 if needed)
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hms;
    
  }
  
function displayDrejtim($nisje, $destinacion)
{
	list($distances, $prev) = $_SESSION["graf"]->paths_from($nisje);
	
	$path = $_SESSION["graf"]->paths_to($prev, $destinacion);
		
	$recordset="";
	
	
	$line="";
	
	$nr_stacione_ndryshim=1;
	$ngjyra_harte='';
	$bus=0;
	$icon='';
	
	$nr_total_stacione=count($path); //nga dijkstra - ok;
	$koha_totale=0; //nga koha e grafit;
	$kosto_totale=0; //nga ndryshimi i linjave;
	$distanca_totale=0; //nga distanca e grafit;
	$nr_total_linja=1; //nga ndryshimi i linjave - ok;
	$drejtimi='';
	$pesha=0;
	
	if($nr_total_stacione>0)
	{
	$query="SELECT grafi.id AS id, grafi.nisje_stacion_id AS nisje_stacion_id, grafi.destinacion_stacion_id AS destinacion_stacion_id, grafi.drejtim_id AS grafi_drejtim_id, grafi.koha AS koha, grafi.distanca AS distanca, grafi.pesha AS pesha, grafi.bus AS bus, stacion_nisje.emer AS nisje_stacion_emer, stacion_destinacion.emer AS destinacion_stacion_emer, drejtim_destinacion.drejtimi AS destinacion_drejtimi, linja_destinacion.id AS destinacion_linja_id, linja_destinacion.emer AS destinacion_linja_emer, linja_destinacion.cmimi AS destinacion_linja_cmimi, linja_destinacion.ngjyra_harte AS destinacion_linja_ngjyra_harte
			FROM grafi INNER JOIN stacion AS stacion_nisje ON grafi.nisje_stacion_id = stacion_nisje.id INNER JOIN stacion AS stacion_destinacion ON grafi.destinacion_stacion_id = stacion_destinacion.id INNER JOIN drejtim AS drejtim_destinacion ON grafi.drejtim_id = drejtim_destinacion.id INNER JOIN linja AS linja_destinacion ON drejtim_destinacion.linja_id = linja_destinacion.id
			GROUP BY  grafi.id, grafi.nisje_stacion_id, grafi.destinacion_stacion_id, grafi.koha, grafi.distanca, grafi.pesha, grafi.bus, stacion_nisje.emer, stacion_destinacion.emer
			HAVING nisje_stacion_emer='".$path[0]."' AND destinacion_stacion_emer='".$path[1]."'";
	
	//echo "<p><br><br>".$query.";<br><br><p>";
	
	$resource=mysql_query($query);
	if($resource)
    {
    	while($recordset=@mysql_fetch_array($resource))
    	{
			$line=$recordset["destinacion_linja_emer"];
			$ngjyra_harte=$recordset["destinacion_linja_ngjyra_harte"]; //ngjyra ka gabim, swap
			$bus=$recordset["bus"];
			$pesha+=$recordset["pesha"];
			
			$koha_totale+=$recordset["koha"]; 
			$kosto_totale+=$recordset["destinacion_linja_cmimi"];
			$distanca_totale+=$recordset["distanca"];
			$drejtimi=$recordset["destinacion_drejtimi"];
			
			//echo $drejtimi;
    	}
    }
    
    switch ($bus)
	{
		case 1: $icon="btype1.gif"; break;
		case 2: $icon="btype2.gif"; break;
		case 3: $icon="btype3.gif"; break;
		case 4: $icon="btype4.gif"; break;
		case 5: $icon="btype5.gif"; break;
		case 6: $icon="btype6.gif"; break;
	}
	
	
	//echo "<br>".$line."<br>";
	for ($i=1; $i<$nr_total_stacione-1; $i++)	
	{
		//echo "testing: ".$path[$i]." ";
		$query="SELECT grafi.id AS id, grafi.nisje_stacion_id AS nisje_stacion_id, grafi.destinacion_stacion_id AS destinacion_stacion_id, grafi.drejtim_id AS grafi_drejtim_id, grafi.koha AS koha, grafi.distanca AS distanca, grafi.pesha AS pesha, grafi.bus AS bus, stacion_nisje.emer AS nisje_stacion_emer, stacion_destinacion.emer AS destinacion_stacion_emer, drejtim_destinacion.drejtimi AS destinacion_drejtimi, linja_destinacion.id AS destinacion_linja_id, linja_destinacion.emer AS destinacion_linja_emer, linja_destinacion.cmimi AS destinacion_linja_cmimi, linja_destinacion.ngjyra_harte AS destinacion_linja_ngjyra_harte
				FROM grafi INNER JOIN stacion AS stacion_nisje ON grafi.nisje_stacion_id = stacion_nisje.id INNER JOIN stacion AS stacion_destinacion ON grafi.destinacion_stacion_id = stacion_destinacion.id INNER JOIN drejtim AS drejtim_destinacion ON grafi.drejtim_id = drejtim_destinacion.id INNER JOIN linja AS linja_destinacion ON drejtim_destinacion.linja_id = linja_destinacion.id
				GROUP BY  grafi.id, grafi.nisje_stacion_id, grafi.destinacion_stacion_id, grafi.koha, grafi.distanca, grafi.pesha, grafi.bus, stacion_nisje.emer, stacion_destinacion.emer
				HAVING nisje_stacion_emer='".$path[$i]."' AND destinacion_stacion_emer='".$path[$i+1]."'";
		
		//echo "<p><br><br>".$query.";<br><br><p>";
		
		$resource=mysql_query($query);
		if($resource)
    	{
    		while($recordset=@mysql_fetch_array($resource))
    		{
    			$koha_totale+=$recordset["koha"]; 
				$distanca_totale+=$recordset["distanca"];
				$pesha+=$recordset["pesha"];
					
    			//echo "<br>".$recordset["destinacion_linja_emer"]."<br>";
				if($path[$i]==$recordset["nisje_stacion_emer"] && ($line<>$recordset["destinacion_linja_emer"] ))
				{
					$kosto_totale+=$recordset["destinacion_linja_cmimi"];
					$nr_total_linja++;
					$test_afishim.= '<!-- blloku '.$i.' -->	
						    <div class="routedesc">
								<div class="routeicon">
									<img width="9" height="9" alt="" class="baseline" src="./imetro/'.$icon.'">&nbsp;
						         </div>
								<span class="txtsmu">Merr Linjen "'.$line.'"</span>
							</div>
						
							<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">Drejtimi
							<span class="txtsmi">&nbsp;'.$recordset["destinacion_drejtimi"].'<br></span>
							</div>
							<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">
								Vijo per
				        		<span class="txtsmb">'.$nr_stacione_ndryshim.' stacion(e)</span>
				        		<!-- <div> -->
							</div>
						    <!-- fund blloku i '.$i.' -->';
					
					
					$test_afishim.=  '<div class="routedesc">
						    	<div class="routeicon">
						    		<img src="./imetro/bball.gif" width="9" height="10" alt="" class="baseline">&nbsp;
						        </div>
						        Qendro tek 
						        <!-- </div> -->
						        <span class="txtsmb">'.$recordset["nisje_stacion_emer"].'
						        <input type="image" src="./imetro/maps.gif" alt="Google Map" title="Google Map" class="top noprint" onclick="showMap(&#39;Tirana&#39;, &#39;Deshmoret e 4 Shkurtit&#39;, 16.38445, 48.20637);">
						        </span>
						        <!-- <div> -->
						    </div>';
					
					$bus=$recordset["bus"]; //???shikoje kete???
					if($recordset["bus"]==0)
					{
						$test_afishim.=  '<div class="routedesc">
						    	<div class="routeicon">
						    		<img src="./imetro/bfoot.gif" width="9" height="10" alt="" class="baseline">&nbsp;
						        </div>
						        Ec drejt 
						        <!-- </div> -->
						        <span class="txtsmb">'.$recordset["destinacion_stacion_emer"].'
						        <input type="image" src="./imetro/maps.gif" alt="Google Map" title="Google Map" class="top noprint" onclick="showMap(&#39;Tirana&#39;, &#39;Deshmoret e 4 Shkurtit&#39;, 16.38445, 48.20637);">
						        </span>
						        <!-- <div> -->
						    </div>';
					}
										
					
					
					$line=$recordset["destinacion_linja_emer"];
					$drejtimi=$recordset["destinacion_drejtimi"];
					$ngjyra_harte=$recordset["destinacion_linja_ngjyra_harte"];
					
					$nr_stacione_ndryshim=0;
					break;
				}
				else if($recordset["bus"]==0)
				{
					$test_afishim.=  '<div class="routedesc">
						    	<div class="routeicon">
						    		<img src="./imetro/bfoot.gif" width="9" height="10" alt="" class="baseline">&nbsp;
						        </div>
						        Ec drejt 
						        <!-- </div> -->
						        <span class="txtsmb">'.$recordset["destinacion_stacion_emer"].'
						        <input type="image" src="./imetro/maps.gif" alt="Google Map" title="Google Map" class="top noprint" onclick="showMap(&#39;Tirana&#39;, &#39;Deshmoret e 4 Shkurtit&#39;, 16.38445, 48.20637);">
						        </span>
						        <!-- <div> -->
						    </div>';
				}			
    		}
    	}
    	else {echo mysql_error();}
    	
    	//echo "<br>";
    	$nr_stacione_ndryshim++;
	}
	}

	echo '<div id="content">
	<ul class="pageitem">
		<li class="textbox">
			<span class="header">Rruga me eficente</span>
			<p style="background-color:black; text-align: center"><font color="white">'.$nr_total_stacione.' stacion(e) - '.$nr_total_linja.' linje(a)<br />Koha: ~'.sec2hms($koha_totale).' Distanca: '.$distanca_totale.'m<br>Total: '.$kosto_totale.' leke<br /></font></p>
		</li>
	</ul>
	
	<ul class="pageitem">
		<li class="textbox">
			<span class="header">Itinerari&nbsp;&nbsp;&nbsp;&nbsp;
			</span>
		</li>
		
		<li class="textbox">
			<ul class="pageitem">';
				
				
			


		echo '<div class="empty txtsml">
				    
				    <!-- stacioni i nisjes -->
					<div class="routedesc">
						<div class="routeicon">
							<img src="./imetro/bball.gif" width="8" height="10" alt="" class="baseline">&nbsp;
				        </div>
				        Tek
				        <span class="txtsmb">'.$nisje.'</span><!-- <div> -->
				        <input type="image" src="./imetro/maps.gif" alt="Google Map" title="Google Map" class="top noprint" onclick="showMap(&#39;Tirana&#39;, &#39;Deshmoret e 4 Shkurtit&#39;, 16.38445, 48.20637);">
				    </div>
					<!-- fund stacioni i nisjes -->';
	
		if($nr_total_stacione>0)
		{
			if($bus==0)
			{
				$nr_stacione_ndryshim--;
			}
		  	echo $test_afishim;
		
					echo '<!-- blloku '.$i.' -->	
						    <div class="routedesc">
								<div class="routeicon">
									<img width="9" height="9" alt="" class="baseline" src="./imetro/'.$icon.'">&nbsp;
						         </div>
								<span class="txtsmu">Merr Linjen "'.$line.'"</span>
							</div>

							<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">Drejtimi
								<span class="txtsmi">&nbsp;'.$drejtimi.'<br></span>
							</div>
							
							<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">
								Vijo per
				        		<span class="txtsmb">'.$nr_stacione_ndryshim.' stacion(e)</span>
				        		<!-- <div> -->
							</div>
						    <!-- fund blloku i '.$i.' -->';
		}
		else 
		{
			echo '<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">
					Nuk u gjet asnje rruge<br>
							</div>
							
				 	<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">
				 	transporti publik<br>
							</div>
							
					<div class="routeline" style="border-left-color: #'.$ngjyra_harte.';">
					qe te pershkoje stacionet e zgjedhura.<br>
							</div>
					';
		}
					
					//include("imetro.php");


					echo '<!-- stacioni i destinacionit -->
				    <div class="routedesc">
				    	<div class="routeicon">
				    		<img src="./imetro/bball.gif" width="8" height="10" alt="" class="baseline">&nbsp;
				        </div>
				        Zbrit tek 
				        <!-- </div> -->
				        <span class="txtsmb">'.$destinacion.'</span>
				        <input type="image" src="./imetro/maps.gif" alt="Google Map" title="Google Map" class="top noprint" onclick="showMap(&#39;Tirana&#39;, &#39;Deshmoret e 4 Shkurtit&#39;, 16.38445, 48.20637);">
				    </div>
				    <!-- stacioni i destinacionit -->

				</div>
			</ul>
		</li>
	</ul>
</div>';
					
//echo "<br><br>".$pesha;
				
				
return $path;
}

?>

<div id="footer"><a class="noeffect" href="#"><?php //print_r($path); ?></a></div>


<div id="footer">
	<!-- Support iWebKit by sending us traffic; please keep this footer on your page, consider it a thank you for my work :-) -->
	<a class="noeffect" href="http://www.juliankanini.com/">iPhone / Android site powered by J.Kanini<sup>2</sup>.</a>
</div>

</body>
</html>
