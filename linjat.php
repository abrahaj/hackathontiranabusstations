<?php
/*
* Author: julian kanini
*/
	 require_once("sysconfig.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />

<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<link rel="stylesheet" type="text/css" href="./imetro/imetro.css">

<script src="javascript/functions.js" type="text/javascript"></script>
<title>Tirana Bus Stations</title>
<meta content="keyword1,keyword2,keyword3" name="keywords" />
<meta content="Description of your page" name="description" />
</head>

<body>

<div id="topbar" class="transparent">
	<div id="title">Linjat</div>
<?php
include("mainmenu.php");
?>

			<?php 
			
			switch ($_GET["mtd"])
			{
				case "map":
					echo '<div id="content">
						<span class="graytitle">Harta</span>
							<ul class="pageitem">
							 <li class="textbox">
								<p>';
					echo '<iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=http:%2F%2Ftiranabusstations.webege.com%2FTirana%2520Bus.kml&amp;aq=&amp;sll=13.731381,100.638885&amp;sspn=0.715042,1.234589&amp;vpsrc=0&amp;ie=UTF8&amp;t=m&amp;ll=41.320365,19.815284&amp;spn=0.093627,0.134703&amp;output=embed"></iframe><br />';
					
					echo '
									</p>
								</li>
							</ul>
						</div>';
					break;
				case "linja":
					shfaqLinje($_GET["id"]); break;
				default: listoLinja(); break;
			}
			
			
			?>


<div id="footer">
	<!-- Support iWebKit by sending us traffic; please keep this footer on your page, consider it a thank you for my work :-) -->
	<a class="noeffect" href="http://www.juliankanini.com/">iPhone / Android site powered by J.Kanini<sup>2</sup>.</a></div>

</body>

</html>

<?php 

function shfaqLinje($id)
{
	$query="SELECT drejtim.id AS drejtim_id, drejtim.drejtimi AS drejtimi, linja.id AS linja_id, linja.emer AS linja_emer, drejtim.pershkrim AS drejtimi_pershkrim, linja.cmimi AS cmimi, linja.frekuenca AS frekuenca, linja.s_time AS s_time, linja.e_time AS e_time, linja.ngjyra_harte AS ngjyra_harte
			FROM drejtim INNER JOIN linja ON drejtim.linja_id = linja.id
			WHERE linja_id=".$id;
	
	//echo $query;
	
	$sort='ASC';
	
	$resource=mysql_query($query);
	if($resource)
    {
    	if(mysql_num_rows($resource)>0)
    	{
	    	while($recordset=@mysql_fetch_array($resource))
	    	{
	    		echo '
						<div id="content">
						<span class="graytitle">'.$recordset["linja_emer"].' - '.$recordset["drejtimi"].'</span>
						<ul class="pageitem">
						
						<li class="textbox">
							<span class="header">Statistika</span>
							<p>'.$recordset["drejtimi_pershkrim"].'</p>
						</li>
						
							<li class="textbox">
							<span class="header">Linjat</span>
								<p>';
	    		
				displayStacione($recordset["drejtim_id"], $sort);
				
				
				echo '
								</p>
							</li>
						</ul>
					</div>';
	    	}
    	}
    	else
    	{
    		echo '<div id="content">
					<span class="graytitle">Kujdes!</span>
					<ul class="pageitem">
						<li class="textbox">
							<span class="header">Nuk u gjeten stacione!</span>
							<p>Fatkeqsisht asnje stacion nuk u gjet per linjen e kerkuar. Nese problemi perseritet, ju ftojme te kontaktoni autoret sa me pare.</p>
						</li>
					</ul>
					
				</div>';
    	}
    }
}

function displayStacione($id, $sort)
{
		$query="SELECT stacion.id AS stacion_id, stacion.emer AS stacion_emer, drejtim.id AS drejtim_id, drejtim.drejtimi AS drejtimi, linja.id AS linja_id, linja.emer AS linja_emer, linja.pershkrim AS linja_pershkrim, linja.cmimi AS cmimi, linja.frekuenca AS frekuenca, linja.s_time AS s_time, linja.e_time AS e_time, linja.ngjyra_harte AS ngjyra_harte, drejtim_stacion.rend AS stacion_rend
				FROM stacion INNER JOIN drejtim_stacion ON drejtim_stacion.stacion_id = stacion.id INNER JOIN drejtim ON drejtim_stacion.drejtim_id = drejtim.id INNER JOIN linja ON drejtim.linja_id = linja.id
				WHERE drejtim.id = ".$id."
				ORDER BY stacion_rend ".$sort.", stacion_id ".$sort;
			
			//echo $query."<br>";
			
			$resource=mysql_query($query);
			if($resource)
		    {
		    	echo '<table >';
		    	while($recordset=@mysql_fetch_array($resource))
		    	{
		    		echo '<tr>
				    	<td bgcolor="#'.$recordset["ngjyra_harte"].'" width="5px">
				            <p style="border-bottom-style:solid; border-bottom-color:#000000; border-top-style:solid; border-top-color:#000000">&nbsp;</p>
				        </td>
				        <td width="100%">
				            <span class="txtsmb">'.$recordset["stacion_id"].' - '.$recordset["stacion_emer"].'</span>
				        </td>';
				        
		    		checkConnections($recordset["stacion_id"], $recordset["linja_id"]);
				        
				    echo '</tr>';
		    	}
		    	
		    	echo '</table>';
		    }
		    else {echo mysql_error();}
}

function checkConnections($id, $linja_id)
{
	$query='SELECT stacion.id AS stacion_id, drejtim.linja_id, linja.ngjyra_harte
			FROM stacion INNER JOIN drejtim_stacion ON drejtim_stacion.stacion_id = stacion.id INNER JOIN drejtim ON drejtim_stacion.drejtim_id = drejtim.id INNER JOIN linja ON drejtim.linja_id = linja.id
			WHERE stacion.id='.$id.' and linja_id<>'.$linja_id.'
			GROUP BY stacion.id, drejtim.linja_id, linja.ngjyra_harte';
	
	//echo $query."<br><br>";
	
	$resource=mysql_query($query);
	if($resource)
	{
		while($recordset=@mysql_fetch_array($resource))
		{
			echo '<td bgcolor="#'.$recordset["ngjyra_harte"].'" width="5px">
					<p style="border-bottom-style:solid; border-bottom-color:#000000; border-top-style:solid; border-top-color:#000000"><a href="?mtd=linja&id='.$recordset["linja_id"].'">&nbsp;&nbsp;</a></p>
				</td>';
		}
	}
}

function listoLinja()
{
	
echo '<div id="content">
		<span class="graytitle">Linjat</span>
			<ul class="pageitem">
			 <li class="textbox">
				<p>
				<ul class="pageitem">
					<li class="menu">
						<a href="linjat.php?mtd=map">
							<img alt="Description" src="thumbs/maps.png" />
							<span class="name">Shikoji ne harte</span>
							<span class="arrow"></span>
						</a>
					</li>
				  </ul>';
	
	$grup="";
	$query="SELECT id, emer, ngjyra_harte, rend, grup
			FROM linja ORDER BY linja.grup ASC, linja.rend ASC";
	
	//echo $query."<br>";
	
	$resource=mysql_query($query);
	if($resource)
    {
    	while($recordset=@mysql_fetch_array($resource))
    	{
    		if($grup<>$recordset["grup"])
    		{
	    		switch ($recordset["grup"])
	    		{
	    			case "1_urban": echo '<p><br><br><span class="header">Linjat Urbane Kryesore</span><br></p>'; break;
	    			case "2_interurban": echo '<p><br><br><span class="header">Linjat Inter-Urbane</span><br></p>'; break;
	    			case "3_tregtar_free": echo '<p><br><br><span class="header">Linjat Tregtare Falas</span><br></p>'; break;
	    			case "4_rural": echo '<p><br><br><span class="header">Linjat Rurale</span><br></p>'; break;
	    		}
	    		$grup=$recordset["grup"];
    		}
    		echo '<ul class="pageitem">
					<li class="menu">
						<div class="doublerouteline" style="border-left-color: #'.$recordset["ngjyra_harte"].';">
							<a href="linjat.php?mtd=linja&id='.$recordset["id"].'">
								<span class="name">'.$recordset["emer"].'</span>
								<span class="arrow"></span>
							</a>
						</div>
					</li>
				  </ul>';
    	}
    }
    else {echo mysql_error();}
    
    echo '
							</p>
						</li>
					</ul>
				</div>';
}

?>
