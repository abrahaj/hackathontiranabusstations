<?php
/*
* Author: julian kanini
*/
require_once ("sysconfig.php");

function generateNodes() {
		
	//$query="SELECT * FROM stacione_te_grafit";
	
	$query="SELECT grafi.id AS id, grafi.nisje_stacion_id AS nisje_stacion_id, grafi.destinacion_stacion_id AS destinacion_stacion_id, grafi.drejtim_id AS grafi_drejtim_id, grafi.stacione AS grafi_stacione, grafi.koha AS koha, grafi.distanca AS distanca, grafi.pesha AS pesha, grafi.bus AS bus, stacion_nisje.emer AS nisje_stacion_emer, stacion_destinacion.emer AS destinacion_stacion_emer, drejtim_destinacion.drejtimi AS destinacion_drejtimi, linja_destinacion.id AS destinacion_linja_id, linja_destinacion.emer AS destinacion_linja_emer, linja_destinacion.cmimi AS destinacion_linja_cmimi, linja_destinacion.ngjyra_harte AS destinacion_linja_ngjyra_harte
			FROM grafi INNER JOIN stacion AS stacion_nisje ON grafi.nisje_stacion_id = stacion_nisje.id INNER JOIN stacion AS stacion_destinacion ON grafi.destinacion_stacion_id = stacion_destinacion.id INNER JOIN drejtim_stacion AS drejtim_stacion_destinacion ON drejtim_stacion_destinacion.stacion_id = stacion_destinacion.id INNER JOIN drejtim AS drejtim_destinacion ON drejtim_stacion_destinacion.drejtim_id = drejtim_destinacion.id AND grafi.drejtim_id = drejtim_destinacion.id INNER JOIN linja AS linja_destinacion ON drejtim_destinacion.linja_id = linja_destinacion.id
			
			GROUP BY  grafi.id, grafi.nisje_stacion_id, grafi.destinacion_stacion_id, grafi.koha, grafi.distanca, grafi.pesha, grafi.bus, stacion_nisje.emer, stacion_destinacion.emer";
	
	
	$resource=mysql_query($query);
	
	$_SESSION["rruge"]=$resource;
	
	if($resource)
    	{
    		while ($recordset=@mysql_fetch_array($resource))
    		{
    			$_SESSION["graf"]->addedge($recordset["nisje_stacion_emer"], $recordset["destinacion_stacion_emer"], $recordset["pesha"]);
    		}
    	}
    	else {echo mysql_error();}
	
}


generateNodes();

