<?php 
/*
* Author: julian kanini
*/
require_once("sysconfig.php");
$_SESSION["graf"]="";
$_SESSION["graf_id"]="";
$_SESSION["rruge"]="";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />

<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />

<script src="javascript/functions.js" type="text/javascript"></script>
<title>Tirana Bus Stations</title>
<meta content="keyword1,keyword2,keyword3" name="keywords" />
<meta content="Description of your page" name="description" />
</head>

<body>

<div id="topbar" class="transparent">
	<div id="title">Tirana Bus Stations</div>
<?php
include("mainmenu.php");
?>

<div id="content">
	<span class="graytitle">Tirana Bus Stations</span>
	<ul class="pageitem">
		<li class="textbox">
			<span class="header">Projekti</span>
			<p>Gjeni cilin autobuz duhet te merrni per te vajtur kudo neper Tirane.</p><br>
			<p>Qellimi jone eshte ti pajisim perdoruesit me nje menyre interaktive per tu orientuar me transportin publik ne Tirane. Ky applikacion po kalon neper faza te rendesishme zhvillimi dhe eshte ende i paplote ashtu sic mund te jete subjekt i ndryshimeve te rrenjesishme.</p><br>
			<p>Ne do bejme cmos per te perditesuar shpeshtazi web serviset tona ndaj dhe do mbetemi ne kontakt.</p>
		</li>
	</ul>
	
	
	
	
	
</div>

<div id="footer">
	<!-- Support iWebKit by sending us traffic; please keep this footer on your page, consider it a thank you for my work :-) -->
	<a class="noeffect" href="http://www.juliankanini.com/">iPhone / Android site powered by J.Kanini<sup>2</sup>.</a></div>

</body>

</html>
